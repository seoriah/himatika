<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('mdl_user', 'usermodel');
		$this->template->set_template('users');
	}

	public function index()
	{	
		if ($this->session->userdata('isLogin') == FALSE) {
			redirect('user/login');
		}
		redirect('user/konfirmasi')	;
	}

	public function home()
    {
    	$this->template->set_template('public');
    	if ($this->session->userdata('isLogin') == TRUE) {
    		$this->template->set_template('users');
    	}
		
        $this->template->write_view('content', 'home', TRUE);
        $this->template->render();
    }

	public function login()
  	{
  		if ($this->session->userdata('isLogin') == FALSE) {
			$this->form_validation->set_rules('email', 'Email', 'required|trim', array('required' => '* %s tidak boleh kosong'));
		    $this->form_validation->set_rules('password', 'Password', 'required|trim', array('required' => '* %s tidak boleh kosong'));
		    $this->form_validation->set_error_delimiters('<span class="text-error">', '</span>');

	    	if($this->form_validation->run() == FALSE){
		    	$this->template->set_template('public');
		        $this->template->write_view('content', 'users/login', TRUE);
		        $this->template->render();

	  		}else{
		  		$data = array(
		      		'email' => $this->input->post('email'),
		      		'password' => sha1($this->input->post('password'))
		      	);
		  		$this->do_login($data);
	        }
		}else{
			redirect('user/home');
		}
	  	
	}

	private function do_login($data = array())
	{
		$result =  $this->usermodel->login($data);
		if(isset($result))
        {	
        	$session = array(
        		'isLogin' => TRUE, 
        		'id_user' => $result->id_user
        	);
        	$this->session->set_userdata($session);
        	redirect('user/konfirmasi');
   		}else{
        	echo " 
        		<script>
		            alert('Pastikan Email dan Password Anda benar!');
		            history.go(-1);
	        	</script>"; 
	    }

	}

	public function register()
	{
		
		// $this->form_validation->set_rules('nama', 'Nama', 'required|trim', array('required' => '* %s tidak boleh kosong'));
		// $this->form_validation->set_rules('alamat', 'Alamat', 'required|trim', array('required' => '* %s tidak boleh kosong'));
		// $this->form_validation->set_rules('email', 'Email', 'required|trim', array('required' => '* %s tidak boleh kosong'));
	 //    $this->form_validation->set_rules('password', 'Password', 'required|trim', array('required' => '* %s tidak boleh kosong'));
	 //    $this->form_validation->set_error_delimiters('<span class="text-error">', '</span>');

	 //   if($this->form_validation->run() == FALSE){

				$this->template->set_template('public');
		        $this->template->write_view('content', 'users/register', TRUE);
		        $this->template->render();
	 //    }else{  

		// 	$n_depan = $this->input->post('n_depan');
		// 	$n_belakang = $this->input->post('n_belakang');
		// 	$kelamin = $this->input->post('kelamin');
		// 	$alamat = $this->input->post('alamat');
		// 	$dd = $this->input->post('dd');
		// 	$mm = $this->input->post('mm');
		// 	$yyyy = $this->input->post('yyyy');
		// 	$n_sekolah = $this->input->post('s_nama');
		// 	$a_sekolah = $this->input->post('s_alamat');
		// 	$telp = $this->input->post('telp');
		// 	$email = $this->input->post('email');
		// 	$password = sha1($this->input->post('password'));


			
		// 	$result = $this->usermodel->cek_user($email);
		// 	if (isset($result)) {
		// 		echo " <script>
		//             alert('Email sudah didaftarkan');
		//             history.go(-1);
		//           </script>";
		// 	}else{
		// 		$data = array(
		// 			'nama' => $n_depan.' '.$n_belakang,
		// 			'kelamin' => $kelamin,
		// 			'tgl_lahir' => $dd.'-'.$mm.'-'.$yyyy,
		// 			'alamat' => $alamat,
		// 			'nama_sekolah' => $n_sekolah,
		// 			'alamat_sekolah' => $a_sekolah,
		// 			'telp' => $telp,
		// 			'email' => $email,
		// 			'password' => $password,
		// 			'status' => 't');
		// 		$this->do_register($data);	
		// 	}
		// }
	}

	public function do_register($data = array())
	{
			$n_depan = $this->input->post('n_depan');
			$n_belakang = $this->input->post('n_belakang');
			$kelamin = $this->input->post('kelamin');
			$alamat = $this->input->post('alamat');
			$dd = $this->input->post('dd');
			$mm = $this->input->post('mm');
			$yyyy = $this->input->post('yyyy');
			$n_sekolah = $this->input->post('s_nama');
			$a_sekolah = $this->input->post('s_alamat');
			$telp = $this->input->post('telp');
			$email = strtolower($this->input->post('email'));
			$password = sha1($this->input->post('password'));

		$result = $this->usermodel->cek_user($email);
		if (isset($result)) {
			echo " <script>
	            alert('Email sudah didaftarkan');
	            history.go(-1);
	          </script>";
		}else{
			$data = array(
					'nama' => $n_depan.' '.$n_belakang,
					'kelamin' => $kelamin,
					'tgl_lahir' => $dd.'-'.$mm.'-'.$yyyy,
					'alamat' => $alamat,
					'nama_sekolah' => $n_sekolah,
					'alamat_sekolah' => $a_sekolah,
					'telp' => $telp,
					'email' => $email,
					'password' => $password,
					'status' => 't');
			$result = $this->usermodel->register($data);
			if (isset($result)) {
				echo " <script>
			            alert('Success ! Silahkan login ');
			            window.location.href = 'login';
			          </script>";

			}else{
				echo " <script>
			            alert('Error ! Pastikan semua data dengan benar');
			            history.go(-1);
			          </script>";
			}
		}
		
	}

	
	
	public function do_confirm()
	{
		$this->form_validation->set_rules('pengirim', 'Nama Pengirim', 'required|trim', array('required' => '* %s tidak boleh kosong'));
		$this->form_validation->set_rules('bank', 'Bank Pengirim', 'required|trim', array('required' => '* %s tidak boleh kosong'));
		$this->form_validation->set_rules('tanggal', 'Tanggal', 'required|trim', array('required' => '* %s tidak boleh kosong'));
		$this->form_validation->set_error_delimiters('<span class="text-error">', '</span>');

	    if($this->form_validation->run() == FALSE){
	        $this->template->write_view('content', 'users/konfirmasi', TRUE);
	        $this->template->render();
      	}else{  
      		$user = $this->session->userdata('id_user');
			      	if (!is_dir('uploads/'.$user)) {
						mkdir('./uploads/'.$user, 0777, TRUE);

						}
				$config = array(

					'upload_path' => './uploads/'.$user,
					'allowed_types' => 'jpeg|jpg|png',
					'max_size' => '2048',
					'max_width' => '2000',
					'max_height' => '2000'
			);

				$this->load->library('upload');
				$this->upload->initialize($config);
			    if ( ! $this->upload->do_upload('foto'))
			    {
					redirect('user/do_confirm');
			    }
			      else
			    {
			      $file = $this->upload->data();
			      $data = array(
			        'tanggal' => $this->input->post('tanggal'),
			        'pengirim' => $this->input->post('pengirim'),
			        'bank' => $this->input->post('bank'),
			        'foto' => $file['file_name'],
			        'status' => 't',
			        'id_user' => $this->session->userdata('id_user')
				    );
					$this->usermodel->do_confirm($data);
				}
				redirect('user/konfirmasi');

		}

	}

	public function konfirmasi()
	{
		if ($this->session->userdata('isLogin') == TRUE) {
			$id = $this->session->userdata('id_user');   		
			$result = $this->usermodel->cek_pembayaran($id);
   			if (isset($result)) {	
   				if ($result->status == 't') {
   					$data = array(
   						'status' => 'Pembayaran belum dikonfirmasi, mohon menunggu',
   						'desc' => '' );
   				}else{
   					$data = array(
   						'status' => 'Pembayaran dikonfirmasi',
   						'desc' => 'Selamat Anda diterima sebagai peserta' );
   				}
			        $this->template->write_view('content', 'users/status_pembayaran', $data, TRUE);
			        $this->template->render();	
      		}else{
      			$user = $this->usermodel->get_user($id);
      			if (isset($user)) {	
	   				if ($user->status == 't') {
	   					$this->template->write_view('content', 'users/info_pembayaran', TRUE);
		        		$this->template->render();	
	   				}else{
	   					$data = array(
   						'status' => 'Pembayaran dikonfirmasi',
   						'desc' => 'Selamat Anda diterima sebagai peserta' );
   						$this->template->write_view('content', 'users/status_pembayaran', $data, TRUE);
			        	$this->template->render();	
	   				}
				}
			}
			
		}
	}

	public function susunan_acara()
    {
    	if ($this->session->userdata('isLogin') == TRUE) {
	        $this->template->write_view('content', 'users/susunan_acara', TRUE);
	        $this->template->render();
	    }
    }

   
	public function do_upload()
	{
		
		    $config['upload_path']          = 'assets/';
		    $config['allowed_types']        = 'jpg|png';
		    //$config['max_size']             = 100;
		    // $config['max_width']            = 1024;
		    // $config['max_height']           = 768;

		    $this->load->library('upload', $config);
		    if ( ! $this->upload->do_upload('bukti'))
		    {
		      $error = array('error' => $this->upload->display_errors());
		      $this->load->view('user/upload', $error);
		    }
		      else
		    {
		      $data = array('upload_data' => $this->upload->data());
		      $this->load->view('upload_success', $data);
		    }
	
    }
	
	public function profil()
	{
		if ($this->session->userdata('isLogin') == FALSE) {
			redirect('user/home');
		}else{	
			$id = $this->session->userdata('id_user');
			$result = $this->usermodel->get_user($id);
			$data['nama'] = $result->nama;
	        $this->template->write_view('content', 'users/profil', $data, TRUE);
	        $this->template->render();	
		}
	}

	public function delegasi()
	{
		if ($this->session->userdata('isLogin') == FALSE) {
			redirect('login');
		}else{	
			$this->template->set_master_template('dashboard');
			$this->template->write('title', 'Event Himatika 2018 - Delegasi', TRUE);
			$id = $this->session->userdata('id_user');
			$data['peserta'] = $this->mdl_user->get_peserta($id);
			$where = array(
				'id_user' => $id,
				'status' => 't' ); 	
			$peserta = $this->mdl_user->get_count_peserta($where);
			if ($peserta == 0) {
				$data['konfirmasi'] = FALSE;
			}else{
				$data['konfirmasi'] = TRUE;
			}
			$total = 75000 * $peserta;
			$data['biaya'] = $total;
			$this->session->set_userdata('biaya', $total);
			$this->template->write_view('content', 'delegasi', $data, TRUE);
	        $this->template->render();	
		}
	}
	
	public function deldelegasi()
	{
		$id = $this->uri->segment(3);
		$this->mdl_user->deldelegasi($id);
		redirect('delegasi');
	}
	public function adddelegasi($value='')
	{
		if ($this->session->userdata('isLogin') == FALSE) {
			redirect('login');
		}else{	
			$this->form_validation->set_rules('nama', 'Nama', 'required|trim', array('required' => '* %s tidak boleh kosong'));
			$this->form_validation->set_rules('alamat', 'Alamat', 'required|trim', array('required' => '* %s tidak boleh kosong'));
			$this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required|trim', array('required' => '* %s tidak boleh kosong'));
		    $this->form_validation->set_rules('kelamin', 'Kelamin', 'required|trim', array('required' => '* %s tidak boleh kosong'));
		    $this->form_validation->set_error_delimiters('<span class="text-error">', '</span>');

		    if($this->form_validation->run() == FALSE){
				$this->template->set_master_template('dashboard');
				$this->template->write('title', 'Event Himatika 2018 - Add Delegasi', TRUE);
				
				$this->template->write_view('content', 'adddelegasi',  TRUE);
		        $this->template->render();	
	      	}else{  
	      		$nama = $this->input->post('nama');
	      		$kelamin = $this->input->post('kelamin');
	      		$tgl_lahir = $this->input->post('tgl_lahir');
	      		$alamat = $this->input->post('alamat');
	      		$id = $this->session->userdata('id_user');
	      		$data = array(
	      			'nama' => $nama,
	      			'kelamin' => $kelamin,
	      			'tgl_lahir' => $tgl_lahir,
	      			'alamat' => $alamat,
	      			'status' => 't',
	      			'id_user' => $id);

	      		$result = $this->mdl_user->adddelegasi($data);
	      		if (isset($result)) {
	      			redirect('delegasi');
	      		}
	      	}
			
		}
	}
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('user');
	}

	public function error()
  	{
  		$this->load->view('404');
  	}
}
