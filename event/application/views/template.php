<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        {_meta}
        <title>{title}</title>
        <!--[if IE]> 
        <script> (function() { var html5 = ("abbr,article,aside,audio,canvas,datalist,details," + "figure,footer,header,hgroup,mark,menu,meter,nav,output," + "progress,section,time,video").split(','); for (var i = 0; i < html5.length; i++) { document.createElement(html5[i]); } try { document.execCommand('BackgroundImageCache', false, true); } catch(e) {} })(); </script> <![endif]-->
		<!-- template style -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/skel.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/skel-layers.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/init.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
        {_styles}
        {_scripts}
    </head>
    <body>
   


        <!-- Header -->
            <header id="header" class="skel-layers-fixed">
               
                <h1><a href="#">Himatika Event 2018</a></h1>
                <nav id="nav">
                    <ul>
                        <li><a href="home" >Home</a></li>
                        <li><a href="login" id="masuk">Login / Daftar</a></li>
                    </ul>

                </nav>
            </header>
            
        {content}
        <!-- Footer -->
        <footer id="footer">
            <ul class="copyright">
                <li>&copy; 2018 Himatika</li>
            </ul>
        </footer>
        
    </body>
</html>