<!-- Main -->
			<section id="main" class="wrapper style1">
				
<header class="major">
					<h2>SUSUNAN ACARA</h2>
				</header>

				<div class="container">
						<div class="12u">
							<h3 >Rabu 25 April 2018</h3>
					<table class="alt">
						<thead>
							<th>WAKTU</th>
							<th>ACARA</th>
							<th>PENANGGUNG JAWAB</th>
						</thead>
						<tbody>
							<tr>
								<td>08.00-08.10</td>
								<td>Pembukaan</td>
								<td>Acara</td>
							</tr>
							<tr>
								<td>08.10-08.20</td>
								<td>Pembacaan Ayat Suci</td>
								<td>Ust. Ali Sobri</td>
							</tr>
							<tr>
								<td>08.20-09.00</td>
								<td>Sambutan</td>
								<td>-</td>
							</tr>
							<tr>
								<td>09.00-09.20</td>
								<td>Penutup</td>
								<td>Acara</td>
							</tr>
							<tr>
								<td>09.20-10.00</td>
								<td>Penampilan</td>
								<td>Firhaz</td>
							</tr>
							<tr>
								<td>10.00-13.00</td>
								<td>Babak Penyisihan</td>
								<td>Peserta</td>
							</tr>
							<tr>
								<td>13.00-Selesai</td>
								<td>Ishoma</td>
								<td>Peserta</td>
							</tr>
						</tbody>
						
					</table>
					<h3>Kamis 26 April 2018</h3>
					<table class="alt">
						<thead>
							<th>WAKTU</th>
							<th>ACARA</th>
							<th>PENANGGUNG JAWAB</th>
						</thead>
						<tr>
							<td>08.00-11.00</td>
							<td>Babak Semi Final</td>
							<td>Peserta</td>
						</tr>
						<tr>
							<td>11.10-13.00</td>
							<td>Istirahat</td>
							<td>Peserta</td>
						</tr>
						<tr>
							<td>14.00-14.30</td>
							<td>Babak Final</td>
							<td>Peserta</td>
						</tr>
						<tr>
							<td>14.30-15.00</td>
							<td>Penyerahan Hadiah</td>
							<td>Panitia & Peserta</td>
						</tr>
						<tr>
							<td>15.00-Selesai</td>
							<td>Pemulangan Peserta</td>
							<td>Panitia & Peserta</td>
						</tr>
					</table>

						</div>
				</div>

</section>
