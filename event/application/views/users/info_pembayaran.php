<section id="main" class="wrapper style1">
  <header class="major">
    <h2>INFO PEMBAYARAN</h2>
    <p>Harap diperhatikan dengan benar</p>
  </header>
  <div class="container">
  	<div class="confirm-page">
        <p style="text-align: justify;">Kami hanya menerima pembayaran dengan daftar dibawah ini. Pengiriman diluar ketentuan tidak dalam jaminan kami, mohon perhatikan dengan teliti.</p>
      <h2>BANK BRI</h2>
      <ul class="alt">
        <li>No. Rekening  <b>748-0010-0406-1538</b></li>
        <li>a.n. <b>Hendra Firmansyah</b></li>
      </ul>
      <h2>Biaya <b>Rp. 75.000,-</b></h2>
    <p style="text-align: justify;">Konfirmasi Pembayaran diproses maksimal 3x24 jam.</p>
    <button id="btn-konfirmasi">lanjut konfirmasi</button>
     </div>
  </div>
</section>

<script>
  jQuery(document).ready(function($){
    $('#btn-konfirmasi').click(function() {
    window.location.href = "do_confirm";
    });
   });
</script>