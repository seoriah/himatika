<section id="main" class="wrapper style1">
  <header class="major">
    <h2>KONFIRMASI PEMBAYARAN</h2>
    <p>Isi dengan benar semua kolom</p>
  </header>
  <div class="container">
  	<div class="confirm-page">
       <form action="do_confirm" method="post" enctype="multipart/form-data">
        <label for="pengirim">Pengirim</label>
        <? echo form_error('pengirim')?>
        <input type="text" class="form-control" name="pengirim" placeholder="Nama Lengkap">
        <label for="bank">Bank Pengirim</label>
        <? echo form_error('bank')?>
        <select name="bank" id="" class="form-control">
          <option value="bri">BRI</option>
          <option value="bni">BNI</option>
          <option value="mandiri">Mandiri</option>
          <option value="bca">BCA</option>
          <option value="lainnya">Lainnya</option>

        </select>
        <label for="tanggal">Tanggal Pengiriman</label>
        <? echo form_error('tanggal')?>
        <input type="text" class="form-control" name="tanggal" placeholder="dd-mm-yyyy">
        <label for="bukti">Bukti Pembayaran</label>
        <input type="file" class="form-control" name="foto" placeholder="Bukti Pembayaran">
         <button type="submit" style="margin-top: 10px">kirim</button>
      </form>
     </div>
  </div>
</section>