<section  class="wrapper style1">
  <header class="major">
    <h2>KONFIRMASI PEMBAYARAN</h2>
  </header>
	<div class="container">
		<div class="login-page">
      <? if (!$peserta == FALSE) :?>
		  <table class="alt">
      
        <thead>
          <th>NO</th>
          <th>NAMA</th>
          <th>SEKOLAH</th>
          <th>BUKTI PEMBAYARAN</th>
          <th>OPSI</th>
        </thead>
        <tbody>
          <? 
            $no = 1;
            foreach ($peserta as $row) {
              
              $kelamin = $row->kelamin == 'l' ? 'Pria' : 'Wanita';
              $status = $row->status == 't' ? '<div style="color: blue;">belum dikonfirmasi</div>' : '<div style="color: green;">dikonfirmasi</div>';
              echo "<tr>

              <td>".$no."</td>
              <td><a href='detail_peserta/".$row->id_user."'>".$row->nama."</a></td>
              <td>".$row->nama_sekolah."</td>
              <td class='align-center'><a class='btn' href='show_img/".$row->id_user."'>DOWNLOAD</a></td>
              <td class='align-center'><a  class='btn' href='approve/".$row->id_user."' >KONFIRMASI</a></td>
              </tr>";
              $no++;
            }
          ?>
        </tbody>
      </table>
      <? endif;?>
      <? if (!$pembayaran == FALSE) :?>
      <table class="alt">
      
        <thead>
          <th>NO</th>
          <th>NAMA</th>
          <th>SEKOLAH</th>
          <th>BUKTI PEMBAYARAN</th>
        </thead>
        <tbody>
          <? 
            $no = 1;
            foreach ($pembayaran as $row) {
              
              $kelamin = $row->kelamin == 'l' ? 'Pria' : 'Wanita';
              $status = $row->status == 't' ? '<div style="color: blue;">belum dikonfirmasi</div>' : '<div style="color: green;">dikonfirmasi</div>';
              echo "<tr>

              <td>".$no."</td>
              <td><a href='detail_peserta/".$row->id_user."'>".$row->nama."</a></td>
              <td>".$row->nama_sekolah."</td>
              <td class='align-center'><a class='btn' href='show_img/".$row->id_user."'>DOWNLOAD</a></td>
              </tr>";
              $no++;
            }
          ?>
        </tbody>
      </table>
      <? endif;?>  
		  </div>
		</div>
	</div>
</section>

